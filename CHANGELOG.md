## [1.2.0] - 2022-08-09
### Added
- en_AU translators

## [1.1.0] - 2022-07-07
### Added
- rates cache

## [1.0.2] - 2022-05-20
### Fixed
- links
- texts

## [1.0.0] - 2022-05-16
### Added
- initial version
